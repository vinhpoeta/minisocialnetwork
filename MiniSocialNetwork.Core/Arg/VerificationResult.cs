﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniSocialNetwork.Core.Arg
{
    public class VerificationResult
    {
        public bool IsVerified { get; set; }
        public string Message { get; set; }
    }
}
