﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniSocialNetwork.Core.Arg
{
    public class RegistrationResult
    {
        public bool IsRegistered { get; set; }
        public string Message { get; set; }
    }
}
