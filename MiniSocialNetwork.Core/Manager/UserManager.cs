﻿using MiniSocialNetwork.Core.Arg;
using MiniSocialNetwork.Core.Model.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniSocialNetwork.Core.Model.Manager
{
    public class UserManager
    {
        private PendingUser _userToCreate;

        private User _curUser;

        private IUserService _srvUser;
        private IUserStorage _strUser;
        private IUserSetting _setUser;

        public User Currentuser
        {
            get
            {
                return _curUser;
            }
        }

        public UserManager(IUserService userService, IUserSetting userSetting)//, IUserStorage userStorage
        {
            _srvUser = userService;
            _setUser = userSetting;
            //_strUser = userStorage;
        }

        public async Task<LoginResult> Login(string email, string password)
        {
            try
            {
                var result = await _srvUser.Login(email, password);

                if (result == null)
                {
                    return new LoginResult() { IsLoggedIn = false, Message = "Failed to log the user in." };
                }

                if (result.Item1)
                {
                    _curUser = new User()
                    {
                        Email = email,
                        Key = result.Item2
                    };

                    _setUser.SaveUserKey(result.Item2);

                    return new LoginResult() { IsLoggedIn = true, Message = result.Item2 };
                }

                return new LoginResult() { IsLoggedIn = false, Message = result.Item2 };
            }
            catch (Exception ex)
            {
                return new LoginResult() { IsLoggedIn = false, Message = ex.Message };
            }
        }

        public async Task<RegistrationResult> RegisterUser(
            string email, string password, string confirmationPassword,
            string phoneNumber, string firstName, string lastName)
        {
            var result = await _srvUser.RegisterUser(
                "",
                email,
                password,
                confirmationPassword,
                phoneNumber,
                firstName,
                lastName
            );

            if (result == null)
            {
                return new RegistrationResult() { IsRegistered = false, Message = "Failed to register new user" };
            }

            if (result.Item1)
            {
                _userToCreate = new PendingUser()
                {
                    Email = email,
                    Password = password,
                    ConfirmationPassword = confirmationPassword,
                    FirstName = firstName,
                    LastName = lastName,
                    PhoneNumber = phoneNumber,
                    Key = result.Item2
                };

                return new RegistrationResult() { IsRegistered = true, Message = result.Item2 };
            }

            return new RegistrationResult() { IsRegistered = false, Message = result.Item2 };
        }

        public async Task<VerificationResult> VerifyUser(string verificationCode)
        {
            if (_userToCreate != null)
            {
                var result = await _srvUser.RegisterUserWithVerificationCode(
                    "",
                    _userToCreate.Email,
                    _userToCreate.Password,
                    _userToCreate.ConfirmationPassword,
                    _userToCreate.PhoneNumber,
                    _userToCreate.FirstName,
                    _userToCreate.LastName,
                    verificationCode
                );

                if (result == null)
                {
                    return new VerificationResult() { IsVerified = false, Message = "Failed to verify the user." };
                }

                if (result.Item1)
                {
                    _userToCreate.Password = "";
                    _userToCreate.ConfirmationPassword = "";
                    _curUser = _userToCreate;
                    _curUser.Key = result.Item2;
                    _userToCreate = null;

                    _setUser.SaveUserKey(result.Item2);

                    return new VerificationResult() { IsVerified = true, Message = "New user is verified successfully." };
                }

                return new VerificationResult() { IsVerified = false, Message = result.Item2 };
            }

            return new VerificationResult() { IsVerified = false, Message = "User is not registered." };
        }

        public async Task<LogoutResult> Logout()
        {
            try
            {
                var result = await _srvUser.Logout();

                if (result == null)
                {
                    return new LogoutResult() { IsLoggedOut = false, Message = "Failed to log the user out." };
                }

                if (result.Item1)
                {
                    _curUser = null;

                    _setUser.DeleteUserKey();

                    return new LogoutResult() { IsLoggedOut = true, Message = result.Item2 };
                }

                return new LogoutResult() { IsLoggedOut = false, Message = result.Item2 };
            }
            catch (Exception ex)
            {
                return new LogoutResult() { IsLoggedOut = false, Message = ex.Message };
            }
        }

        /*
         * UserManager's required components
         * */

        public interface IUserService
        {
            Task<Tuple<bool, string>> Login(string email, string password);
            Task<Tuple<bool, string>> Logout();
            Task<Tuple<bool, string>> RegisterUser(
                string username, string email,
                string password, string confirmationPassword,
                string phoneNumber, string firstName, string lastName);
            Task<Tuple<bool, string>> RegisterUserWithVerificationCode(
                string username, string email,
                string password, string confirmationPassword,
                string phoneNumber, string firstName, string lastName,
                string verificationCode);
        }

        // @NOTE: Example
        public interface IUserStorage
        {
            void SaveUserProfile();
        }

        // @NOTE: Example
        public interface IUserSetting
        {
            void SaveUserKey(string key);
            string GetUserKey();
            void DeleteUserKey();
        }
    }
}
