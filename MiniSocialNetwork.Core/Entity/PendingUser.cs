﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniSocialNetwork.Core.Model.Entity
{
    public class PendingUser : User
    {
        public string Password { get; set; }
        public string ConfirmationPassword { get; set; }
    }
}
