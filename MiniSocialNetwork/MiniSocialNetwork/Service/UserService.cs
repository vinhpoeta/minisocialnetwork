﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiniSocialNetwork.Core.Model.Entity;
using static MiniSocialNetwork.Core.Model.Manager.UserManager;
using System.Net.Http;
using Newtonsoft.Json;
using MiniSocialNetwork.Core.Model.Manager;
using Xamarin.Forms;
using MiniSocialNetwork.Service;
using System.Diagnostics;
using System.Net;

[assembly: Dependency(typeof(UserService))]
namespace MiniSocialNetwork.Service
{
    public class UserService : IUserService
    {
        private static HttpClient _apiClient = new HttpClient();

        private const string api_url_main = "http://10.0.3.2:8000";

        private const string api_param_username = "username";
        private const string api_param_email = "email";
        private const string api_param_password = "password";
        private const string api_param_password1 = "password1";
        private const string api_param_password2 = "password2";
        private const string api_param_phone_number = "phone";
        private const string api_param_first_name = "first_name";
        private const string api_param_last_name = "last_name";
        private const string api_param_verification_code = "verify_code";

        private const string api_param_key = "key";

        private const string api_param_errors = "Errors";
        private const string api_param_error_code = "ErrorCode";
        private const string api_param_error_message = "ErrorMessage";

        private const string api_path_login = "rest-auth/login/";
        private const string api_path_logout = "rest-auth/logout/";
        private const string api_path_registration = "rest-auth/registration/";

        public async Task<Tuple<bool, string>> Login(string email, string password)
        {
            try
            {
                var dictParams = new Dictionary<string, string>();
                dictParams.Add(api_param_email, email);
                dictParams.Add(api_param_password, password);

                var loginReqContent = new FormUrlEncodedContent(dictParams);
                var loginResMes = await _apiClient.PostAsync(string.Format("{0}/{1}", api_url_main, api_path_login), loginReqContent);

                if (loginResMes != null && loginResMes.Content != null)
                {
                    var loginResStrContent = await loginResMes.Content.ReadAsStringAsync();
                    var dictLoginRes = JsonConvert.DeserializeObject<IDictionary<string, object>>(loginResStrContent);
                    if (dictLoginRes != null && dictLoginRes.ContainsKey(api_param_key))
                    {
                        return new Tuple<bool, string>(true, dictLoginRes[api_param_key].ToString());
                    }
                    else if (dictLoginRes != null && dictLoginRes.ContainsKey(api_param_errors))
                    {
                        var strErrors = dictLoginRes[api_param_errors].ToString();
                        var arrErrors = JsonConvert.DeserializeObject<object[]>(strErrors);
                        var dictErrors = JsonConvert.DeserializeObject<IDictionary<string, object>>(arrErrors[0].ToString());
                        if (dictErrors.ContainsKey(api_param_error_message))
                        {
                            return new Tuple<bool, string>(false, dictErrors[api_param_error_message].ToString());
                        }
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.StackTrace);
                return null;
            }
        }

        public async Task<Tuple<bool, string>> RegisterUser(
            string username, string email,
            string password, string confirmationPassword,
            string phoneNumber, string firstName, string lastName)
        {
            try
            {
                var dictParams = new Dictionary<string, string>();
                dictParams.Add(api_param_email, email);
                dictParams.Add(api_param_phone_number, phoneNumber);
                dictParams.Add(api_param_password1, password);
                dictParams.Add(api_param_password2, confirmationPassword);
                dictParams.Add(api_param_first_name, firstName);
                dictParams.Add(api_param_last_name, lastName);

                var registerUserReqContent = new FormUrlEncodedContent(dictParams);
                var registerUserResMes = await _apiClient.PostAsync(string.Format("{0}/{1}", api_url_main, api_path_registration), registerUserReqContent);

                if (registerUserResMes != null)
                {
                    switch (registerUserResMes.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            return new Tuple<bool, string>(true, "User was successfully created.");
                        case HttpStatusCode.BadRequest:
                            if (registerUserResMes.Content != null)
                            {
                                var registeruserResStrContent = await registerUserResMes.Content.ReadAsStringAsync();
                                var dictRegisterUserRes = JsonConvert.DeserializeObject<IDictionary<string, object>>(registeruserResStrContent);
                                if (dictRegisterUserRes != null && dictRegisterUserRes.ContainsKey(api_param_errors))
                                {
                                    var strErrors = dictRegisterUserRes[api_param_errors].ToString();
                                    var arrErrors = JsonConvert.DeserializeObject<object[]>(strErrors);
                                    var dictErrors = JsonConvert.DeserializeObject<IDictionary<string, object>>(arrErrors[0].ToString());
                                    if (dictErrors.ContainsKey(api_param_error_message))
                                    {
                                        return new Tuple<bool, string>(false, dictErrors[api_param_error_message].ToString());
                                    }
                                }
                            }
                            break;
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.StackTrace);
                return new Tuple<bool, string>(false, ex.Message);
            }
        }

        public async Task<Tuple<bool, string>> RegisterUserWithVerificationCode(
            string username, string email,
            string password, string confirmationPassword, string phoneNumber,
            string firstName, string lastName,
            string verificationCode)
        {
            try
            {
                var dictParams = new Dictionary<string, string>();
                dictParams.Add(api_param_email, email);
                dictParams.Add(api_param_phone_number, phoneNumber);
                dictParams.Add(api_param_password1, password);
                dictParams.Add(api_param_password2, confirmationPassword);
                dictParams.Add(api_param_first_name, firstName);
                dictParams.Add(api_param_last_name, lastName);
                dictParams.Add(api_param_verification_code, verificationCode);

                var verifyUserReqContent = new FormUrlEncodedContent(dictParams);
                var verifyUserResMes = await _apiClient.PostAsync(string.Format("{0}/{1}", api_url_main, api_path_registration), verifyUserReqContent);

                if (verifyUserResMes != null && verifyUserResMes.Content != null)
                {
                    var verifyUserResStrContent = await verifyUserResMes.Content.ReadAsStringAsync();
                    var dictVerifyUserRes = JsonConvert.DeserializeObject<IDictionary<string, object>>(verifyUserResStrContent);
                    if (dictVerifyUserRes != null && dictVerifyUserRes.ContainsKey(api_param_key))
                    {
                        return new Tuple<bool, string>(true, dictVerifyUserRes[api_param_key].ToString());
                    }
                    else if (dictVerifyUserRes != null && dictVerifyUserRes.ContainsKey(api_param_errors))
                    {
                        var strErrors = dictVerifyUserRes[api_param_errors].ToString();
                        var arrErrors = JsonConvert.DeserializeObject<object[]>(strErrors);
                        var dictErrors = JsonConvert.DeserializeObject<IDictionary<string, object>>(arrErrors[0].ToString());
                        if (dictErrors.ContainsKey(api_param_error_message))
                        {
                            return new Tuple<bool, string>(false, dictErrors[api_param_error_message].ToString());
                        }
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.StackTrace);
                return new Tuple<bool, string>(false, ex.Message);
            }
        }

        public async Task<Tuple<bool, string>> Logout()
        {
            try
            {
                var dictParams = new Dictionary<string, string>();

                var logoutReqContent = new FormUrlEncodedContent(dictParams);
                var logoutResMes = await _apiClient.PostAsync(string.Format("{0}/{1}", api_url_main, api_path_logout), logoutReqContent);

                if (logoutResMes != null && logoutResMes.Content != null)
                {
                    var logoutResStrContent = await logoutResMes.Content.ReadAsStringAsync();
                    var dictLoginRes = JsonConvert.DeserializeObject<IDictionary<string, object>>(logoutResStrContent);
                    if (dictLoginRes != null && dictLoginRes.ContainsKey(api_param_errors))
                    {
                        var strErrors = dictLoginRes[api_param_errors].ToString();
                        var arrErrors = JsonConvert.DeserializeObject<object[]>(strErrors);
                        var dictErrors = JsonConvert.DeserializeObject<IDictionary<string, object>>(arrErrors[0].ToString());
                        if (dictErrors.ContainsKey(api_param_error_message))
                        {
                            return new Tuple<bool, string>(false, dictErrors[api_param_error_message].ToString());
                        }
                    }
                }

                return new Tuple<bool, string>(true, "Log out successfully.");
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.StackTrace);
                return null;
            }
        }
    }
}
