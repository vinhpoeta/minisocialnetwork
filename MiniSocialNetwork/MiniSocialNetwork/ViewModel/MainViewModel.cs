﻿using MiniSocialNetwork.Core.Model.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MiniSocialNetwork.ViewModel
{
    public class MainViewModel : BaseViewModel
    {
        private UserManager _userManager;

        private Command _logoutCommand;

        public Action NavigateToLoginPageAction { get; set; }

        public ICommand LogoutCommand
        {
            get
            {
                return _logoutCommand;
            }
        }

        public MainViewModel()
        {
            _userManager = XamarinLibIoc.Instance.Resolve<UserManager>();
            _logoutCommand = new Command(async () =>
            {
                var logoutResult = await _userManager.Logout();
                if (logoutResult.IsLoggedOut)
                {
                    NavigateToLoginPageAction?.Invoke();
                }
                else
                {
                    // TODO: Show error message dialog
                }
            });
        }
    }
}
