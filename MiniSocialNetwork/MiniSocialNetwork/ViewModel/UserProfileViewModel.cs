﻿using MiniSocialNetwork.Core.Model.Manager;
using MiniSocialNetwork.Util;
using MiniSocialNetwork.View;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MiniSocialNetwork.ViewModel
{
    public class UserProfileViewModel : BaseViewModel
    {
        private UserManager _userManager;

        private string _email;
        private string _firstName;
        private string _lastName;
        private string _phoneNumber;

        private string _popupMessage;

        public String Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
                RaisePropertyChanged();
            }
        }

        public String FirstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                _firstName = value;
                RaisePropertyChanged();
                RaisePropertyChanged("FullName");
            }
        }

        public String LastName
        {
            get
            {
                return _lastName;
            }
            set
            {
                _lastName = value;
                RaisePropertyChanged();
                RaisePropertyChanged("FullName");
            }
        }

        public String FullName
        {
            get
            {
                return string.Format("{0} {1}", _firstName, _lastName);
            }
        }

        public String PhoneNumber
        {
            get
            {
                return _phoneNumber;
            }
            set
            {
                _phoneNumber = value;
                RaisePropertyChanged();
            }
        }

        public String PopupMessage
        {
            get
            {
                return _popupMessage;
            }
            set
            {
                _popupMessage = value;
                RaisePropertyChanged();
            }
        }

        public UserProfileViewModel()
        {
            _userManager = XamarinLibIoc.Instance.Resolve<UserManager>();
        }
    }
}
