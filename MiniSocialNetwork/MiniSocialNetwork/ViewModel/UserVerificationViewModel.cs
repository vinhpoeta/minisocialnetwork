﻿using MiniSocialNetwork.Core.Model.Manager;
using MiniSocialNetwork.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MiniSocialNetwork.ViewModel
{
    public class UserVerificationViewModel : BaseViewModel
    {
        private UserManager _userManager;

        private string _verificationCode;

        private string _popupMessage;

        private Command _cmdVerify;

        public Action NavigateToMainPageAction { get; set; }

        public String VerificationCode
        {
            get
            {
                return _verificationCode;
            }
            set
            {
                _verificationCode = value;
                RaisePropertyChanged();
            }
        }

        public String PopupMessage
        {
            get
            {
                return _popupMessage;
            }
            set
            {
                _popupMessage = value;
                RaisePropertyChanged();
            }
        }

        public ICommand VerifyCommand
        {
            get
            {
                return _cmdVerify;
            }
        }

        public UserVerificationViewModel()
        {
            _userManager = XamarinLibIoc.Instance.Resolve<UserManager>();
            _cmdVerify = new Command(async () =>
            {
                Debug.WriteLine("Verify button clicked! VerificationCode: {0}", VerificationCode);

                var verifyRes = await _userManager.VerifyUser(VerificationCode);
                if (verifyRes.IsVerified)
                {
                    // Navigate to main page
                    NavigateToMainPageAction?.Invoke();
                }
                else
                {
                    PopupMessage = verifyRes.Message;
                }
            });
        }
    }
}
