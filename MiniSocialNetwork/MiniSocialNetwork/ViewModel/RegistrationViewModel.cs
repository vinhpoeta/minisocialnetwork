﻿using MiniSocialNetwork.Core.Model.Manager;
using MiniSocialNetwork.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MiniSocialNetwork.ViewModel
{
    public class RegistrationViewModel : BaseViewModel
    {
        private UserManager _userManager;

        private string _email;
        private string _password;
        private string _confirmationPassword;
        private string _phoneNumber;
        private string _firstName;
        private string _lastName;

        private bool _hasEmailValidated;
        private bool _hasPasswordValidated;
        private bool _hasConfirmationPasswordValidated;
        private bool _hasPhoneNumberValidated;
        private bool _hasFirstNameValidated;
        private bool _hasLastNameValidated;

        private string _emailValidationError;
        private string _passwordValidationError;
        private string _confirmationPasswordValidationError;
        private string _phoneNumberValidationError;
        private string _firstNameValidationError;
        private string _lastNameValidationError;

        private string _popupMessage;

        private Command _cmdRegister;

        public Action NavigateToVerificationPageAction { get; set; }

        public String Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
                RaisePropertyChanged();
                ValidateEmail();
            }
        }

        public String Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
                RaisePropertyChanged();
                ValidatePassword();
            }
        }

        public String ConfirmationPassword
        {
            get
            {
                return _confirmationPassword;
            }
            set
            {
                _confirmationPassword = value;
                RaisePropertyChanged();
                ValidateConfirmationPassword();
            }
        }

        public String PhoneNumber
        {
            get
            {
                return _phoneNumber;
            }
            set
            {
                _phoneNumber = value;
                RaisePropertyChanged();
                ValidatePhoneNumber();
            }
        }

        public String FirstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                _firstName = value;
                RaisePropertyChanged();
                ValidateFirstName();
            }
        }

        public String LastName
        {
            get
            {
                return _lastName;
            }
            set
            {
                _lastName = value;
                RaisePropertyChanged();
                ValidateLastName();
            }
        }

        public String EmailValidationError
        {
            get
            {
                return _emailValidationError;
            }
            set
            {
                _emailValidationError = value;
                RaisePropertyChanged();
                _cmdRegister.ChangeCanExecute();
            }
        }

        public String PasswordValidationError
        {
            get
            {
                return _passwordValidationError;
            }
            set
            {
                _passwordValidationError = value;
                RaisePropertyChanged();
                _cmdRegister.ChangeCanExecute();
            }
        }

        public String ConfirmationPasswordValidationError
        {
            get
            {
                return _confirmationPasswordValidationError;
            }
            set
            {
                _confirmationPasswordValidationError = value;
                RaisePropertyChanged();
                _cmdRegister.ChangeCanExecute();
            }
        }

        public String PhoneNumberValidationError
        {
            get
            {
                return _phoneNumberValidationError;
            }
            set
            {
                _phoneNumberValidationError = value;
                RaisePropertyChanged();
                _cmdRegister.ChangeCanExecute();
            }
        }

        public String FirstNameValidationError
        {
            get
            {
                return _firstNameValidationError;
            }
            set
            {
                _firstNameValidationError = value;
                RaisePropertyChanged();
                _cmdRegister.ChangeCanExecute();
            }
        }

        public String LastNameValidationError
        {
            get
            {
                return _lastNameValidationError;
            }
            set
            {
                _lastNameValidationError = value;
                RaisePropertyChanged();
                _cmdRegister.ChangeCanExecute();
            }
        }

        public String PopupMessage
        {
            get
            {
                return _popupMessage;
            }
            set
            {
                _popupMessage = value;
                RaisePropertyChanged();
            }
        }

        public ICommand RegisterCommand
        {
            get
            {
                return _cmdRegister;
            }
        }

        public RegistrationViewModel()
        {
            _userManager = XamarinLibIoc.Instance.Resolve<UserManager>();
            _cmdRegister = new Command(async () =>
            {
                Debug.WriteLine(
                    "Submit button clicked! " +
                    "Username: {0}, " +
                    "Password: {1}, " +
                    "Confirmation password: {2}, " +
                    "Phone number: {3}, " +
                    "First name: {4}, " +
                    "Last name: {5}",
                    Email, Password, ConfirmationPassword, PhoneNumber, FirstName, LastName);

                var registrationResult = await _userManager.RegisterUser(Email, Password, ConfirmationPassword, PhoneNumber, FirstName, LastName);
                if (registrationResult.IsRegistered)
                {
                    // Navigate to main page
                    NavigateToVerificationPageAction?.Invoke();
                }
                else
                {
                    PopupMessage = registrationResult.Message;
                }
            }, () =>
            {
                return
                _hasEmailValidated &&
                string.IsNullOrWhiteSpace(EmailValidationError) &&
                _hasPasswordValidated &&
                string.IsNullOrWhiteSpace(PasswordValidationError) &&
                _hasConfirmationPasswordValidated &&
                string.IsNullOrWhiteSpace(ConfirmationPasswordValidationError) &&
                _hasPhoneNumberValidated &&
                string.IsNullOrWhiteSpace(PhoneNumberValidationError) &&
                _hasFirstNameValidated &&
                string.IsNullOrWhiteSpace(FirstNameValidationError) &&
                _hasLastNameValidated &&
                string.IsNullOrWhiteSpace(LastNameValidationError);
            });
        }

        private void ValidateEmail()
        {
            _hasEmailValidated = true;

            if (string.IsNullOrWhiteSpace(Email))
            {
                EmailValidationError = "Email must not be empty";
                return;
            }
            if (!FormatUtil.IsMailAddress(Email))
            {
                EmailValidationError = "Invalid email address";
                return;
            }

            EmailValidationError = "";
        }

        private void ValidatePassword()
        {
            _hasPasswordValidated = true;

            if (string.IsNullOrWhiteSpace(Password))
            {
                PasswordValidationError = "Password must not be empty";
                return;
            }
            if (Password.Length < 6)
            {
                PasswordValidationError = "Password length must be greater than six";
                return;
            }

            PasswordValidationError = "";
        }

        private void ValidateConfirmationPassword()
        {
            _hasConfirmationPasswordValidated = true;

            if (ConfirmationPassword != Password)
            {
                ConfirmationPasswordValidationError = "Confirmation password doesn't match password";
                return;
            }

            ConfirmationPasswordValidationError = "";
        }

        private void ValidatePhoneNumber()
        {
            _hasPhoneNumberValidated = true;

            if (string.IsNullOrWhiteSpace(PhoneNumber))
            {
                PhoneNumberValidationError = "Phone number must not be empty";
                return;
            }
            if (!FormatUtil.IsPhoneNumber(PhoneNumber))
            {
                PhoneNumberValidationError = "Invalid phone number";
                return;
            }

            PhoneNumberValidationError = "";
        }

        private void ValidateFirstName()
        {
            _hasFirstNameValidated = true;

            if (string.IsNullOrWhiteSpace(FirstName))
            {
                FirstNameValidationError = "First name must not be empty";
                return;
            }

            FirstNameValidationError = "";
        }

        private void ValidateLastName()
        {
            _hasLastNameValidated = true;

            if (string.IsNullOrWhiteSpace(LastName))
            {
                LastNameValidationError = "Last name must not be empty";
                return;
            }

            LastNameValidationError = "";
        }
    }
}
