﻿using MiniSocialNetwork.Core.Model.Manager;
using MiniSocialNetwork.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MiniSocialNetwork.ViewModel
{
    public class LoginViewModel : BaseViewModel
    {
        private UserManager _userManager;

        private string _email;
        private string _password;

        private bool _hasEmailValidated;
        private bool _hasPasswordValidated;

        private string _emailValidationError;
        private string _passwordValidationError;

        private string _popupMessage;

        private Command _cmdRegister;
        private Command _cmdLogin;

        public Action NavigateToMainPageAction { get; set; }
        public Action NavigateToRegisterPageAction { get; set; }

        public String Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
                RaisePropertyChanged();
                ValidateEmail();
            }
        }

        public String Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
                RaisePropertyChanged();
                ValidatePassword();
            }
        }

        public String EmailValidationError
        {
            get
            {
                return _emailValidationError;
            }
            set
            {
                _emailValidationError = value;
                RaisePropertyChanged();
                _cmdLogin.ChangeCanExecute();
            }
        }

        public String PasswordValidationError
        {
            get
            {
                return _passwordValidationError;
            }
            set
            {
                _passwordValidationError = value;
                RaisePropertyChanged();
                _cmdLogin.ChangeCanExecute();
            }
        }

        public String PopupMessage
        {
            get
            {
                return _popupMessage;
            }
            set
            {
                _popupMessage = value;
                RaisePropertyChanged();
            }
        }

        public ICommand LoginCommand
        {
            get
            {
                return _cmdLogin;
            }
        }

        public ICommand RegisterCommand
        {
            get
            {
                return _cmdRegister;
            }
        }

        public LoginViewModel()
        {
            _userManager = XamarinLibIoc.Instance.Resolve<UserManager>();
            _cmdLogin = new Command(async () =>
            {
                Debug.WriteLine("Login button clicked! Email: {0}, Password: {1}", Email, Password);

                var loginRes = await _userManager.Login(Email, Password);
                if (loginRes.IsLoggedIn)
                {
                    // Navigate to main page
                    NavigateToMainPageAction?.Invoke();
                }
                else
                {
                    PopupMessage = loginRes.Message;
                }
            }, () =>
            {
                // Disable login button if Username or Password field is empty
                return
                _hasEmailValidated &&
                string.IsNullOrWhiteSpace(EmailValidationError) &&
                _hasPasswordValidated &&
                string.IsNullOrWhiteSpace(PasswordValidationError);
            });
            _cmdRegister = new Command(() =>
            {
                NavigateToRegisterPageAction?.Invoke();
            });
        }

        private void ValidateEmail()
        {
            _hasEmailValidated = true;

            if (string.IsNullOrWhiteSpace(Email))
            {
                EmailValidationError = "Email must not be empty";
                return;
            }
            if (!FormatUtil.IsMailAddress(Email))
            {
                EmailValidationError = "Invalid email address";
                return;
            }

            EmailValidationError = "";
        }

        private void ValidatePassword()
        {
            _hasPasswordValidated = true;

            if (string.IsNullOrWhiteSpace(Password))
            {
                PasswordValidationError = "Password must not be empty";
                return;
            }
            if (Password.Length < 6)
            {
                PasswordValidationError = "Password length must be greater than six";
                return;
            }

            PasswordValidationError = "";
        }
    }
}
