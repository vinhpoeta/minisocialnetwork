﻿using System;
using Autofac;
using System.Diagnostics;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;
using Autofac.Core;

namespace MiniSocialNetwork
{
    public class DependencyAttribute : Attribute
    {
        private string _key;

        public string Key
        {
            get
            {
                return _key;
            }
            set
            {
                _key = value;
            }
        }

        public DependencyAttribute(string key)
        {
            _key = key;
        }
    }

    public class XamarinLibIoc
    {
        static Lazy<XamarinLibIoc> _instance = new Lazy<XamarinLibIoc>(() => new XamarinLibIoc());
        IContainer _container;
        ContainerBuilder _builder;
        ContainerBuilder _updater;

        public static XamarinLibIoc Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        #region Container section

        public void CommitRegister()
        {
            try
            {
                _container = _builder.Build();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.StackTrace);
            }
        }

        //public TInterface Resolve<TInterface>(bool withDpdc = false, bool withRecursion = false)
        //{
        //    var result = _container.Resolve<TInterface>();

        //    if (withDpdc)
        //    {
        //        var resultProps = result.GetType().GetRuntimeProperties();
        //        var dependencyAttrs = resultProps.Select(
        //            p =>
        //            new
        //            {
        //                Dependency = p.GetCustomAttribute<DependencyAttribute>(),
        //                Property = p
        //            }
        //        );

        //        if (dependencyAttrs != null)
        //        {
        //            foreach (var dependencyAttr in dependencyAttrs)
        //            {
        //                if (string.IsNullOrWhiteSpace(dependencyAttr.Dependency.Key))
        //                {
        //                    var propVal = GetType().GetRuntimeMethod(
        //                        "Resolve",
        //                        new Type[] {
        //                            typeof(bool),
        //                            typeof(bool)
        //                        }
        //                    )
        //                    .MakeGenericMethod(
        //                        new Type[] {
        //                            dependencyAttr.Property.GetType()
        //                        }
        //                    )
        //                    .Invoke(
        //                        this,
        //                        new object[] {
        //                            withRecursion,
        //                            withRecursion
        //                        }
        //                    );

        //                    dependencyAttr.Property.SetValue(result, )
        //                }
        //                else
        //                {

        //                }
        //            }
        //        }
        //    }

        //    return result;
        //}

        public TInterface Resolve<TInterface>()
        {
            return _container.Resolve<TInterface>();
        }

        public TInterface ResolveKeyed<TInterface>(object key)
        {
            return _container.ResolveKeyed<TInterface>(key);
        }

        public void Register<TInterface, TImplementation>(TImplementation instance)
            where TImplementation : class, TInterface
        {
            var registra = _builder.RegisterInstance(instance).As<TInterface>();

            if (_container != null)
            {
                try
                {
                    _builder.Update(_container);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.StackTrace);
                }
            }
            else
            {

            }

        }

        public void RegisterKeyed<TInterface, TImplementation>(TImplementation instance, object key)
            where TImplementation : class, TInterface
        {
            var registra = _builder.RegisterInstance(instance).Keyed<TInterface>(key);

            if (_container != null)
            {
                try
                {
                    _builder.Update(_container);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.StackTrace);
                }
            }
        }

        public void RegisterModule(IModule module)
        {
            var registra = _builder.RegisterModule(module);

            if (_container != null)
            {
                try
                {
                    _builder.Update(_container);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.StackTrace);
                }
            }
        }

        public void Register<TInterface, TImplementation>(bool singleTon = false)
            where TImplementation : class, TInterface
        {
            var registra = _builder.RegisterType<TImplementation>().As<TInterface>();
            if (singleTon)
            {
                registra.SingleInstance();
            }

            if (_container != null)
            {
                try
                {
                    _builder.Update(_container);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.StackTrace);
                }
            }
        }

        public void RegisterKeyed<TInterface, TImplementation>(object key, bool singleTon = false)
            where TImplementation : TInterface
        {
            var registra = _builder.RegisterType<TImplementation>().Keyed<TInterface>(key);
            if (singleTon)
            {
                registra.SingleInstance();
            }

            if (_container != null)
            {
                try
                {
                    _builder.Update(_container);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.StackTrace);
                }
            }
        }

        public void Register<TType>(bool singleTon = false)
            where TType : class
        {
            var registra = _builder.RegisterType<TType>();
            if (singleTon)
            {
                registra.SingleInstance();
            }

            if (_container != null)
            {
                try
                {
                    _builder.Update(_container);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.StackTrace);
                }
            }
        }

        public void RegisterKeyed<TType>(object key, bool singleTon = false)
            where TType : class
        {
            var registra = _builder.RegisterType<TType>().Keyed<TType>(key);
            if (singleTon)
            {
                registra.SingleInstance();
            }

            if (_container != null)
            {
                try
                {
                    _builder.Update(_container);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.StackTrace);
                }
            }
        }

        public void Dispose()
        {
            if (_container != null)
            {
                _container.Dispose();
                _container = null;
            }

            _builder = null;
            _updater = null;
        }

        public void Register<TInterface, TImplementation>(TImplementation instance, bool singleTon = false)
            where TImplementation : class, TInterface
        {
            throw new NotImplementedException();
        }

        public void Register<TInstance>(TInstance instance, bool singleTon = false)
        {
            throw new NotImplementedException();
        }

        public object Resolve(Type type)
        {
            return _container.Resolve(type);
        }

        public IEnumerable<IType> ResolveAll<IType>()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<object> ResolveAll(Type type)
        {
            throw new NotImplementedException();
        }

        public void Commit()
        {
            try
            {
                _container = _builder.Build();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.StackTrace);
            }
        }

        public void Update()
        {
            throw new NotImplementedException();
        }

        #endregion

        private XamarinLibIoc()
        {
            _builder = new ContainerBuilder();
        }
    }
}

