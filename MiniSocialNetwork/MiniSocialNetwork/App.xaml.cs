﻿using Autofac;
using Autofac.Core;
using MiniSocialNetwork.Core.Model.Manager;
using MiniSocialNetwork.Service;
using MiniSocialNetwork.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using static MiniSocialNetwork.Core.Model.Manager.UserManager;

namespace MiniSocialNetwork
{
    public partial class App : Application
    {
        public static BaseNavigationPage NavigationPage { get; set; } = new BaseNavigationPage();
        public static IContainer DefaultContainer { get; private set; }

        public App(IModule[] platformModules = null)
        {
            InitializeContainer(platformModules);

            InitializeComponent();

            MainPage = NavigationPage;

            var userKey = XamarinLibIoc.Instance.Resolve<IUserSetting>().GetUserKey();
            if (string.IsNullOrWhiteSpace(userKey))
            {
                NavigationPage.PushAsync(new LoginPage());
            }
            else
            {
                NavigationPage.PushAsync(new MainPage());
            }
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        private void InitializeContainer(IModule[] modules)
        {
            if (modules != null)
            {
                foreach (var module in modules)
                {
                    XamarinLibIoc.Instance.RegisterModule(module);
                }
            }

            XamarinLibIoc.Instance.Register<IUserService, UserService>(true);
            XamarinLibIoc.Instance.Register<UserManager>(true);

            XamarinLibIoc.Instance.CommitRegister();
        }
    }
}
