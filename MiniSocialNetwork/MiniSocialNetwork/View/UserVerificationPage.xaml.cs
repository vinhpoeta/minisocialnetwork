﻿using MiniSocialNetwork.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MiniSocialNetwork.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserVerificationPage : ContentPage
    {
        private UserVerificationViewModel _userVerificationVM;

        public UserVerificationPage()
        {
            InitializeComponent();

            _userVerificationVM = BindingContext as UserVerificationViewModel;
            if (_userVerificationVM != null)
            {
                _userVerificationVM.PropertyChanged += UserVerificationViewModel_PropertyChanged;
                _userVerificationVM.NavigateToMainPageAction = delegate
                {
                    App.NavigationPage.PushAsync(new MainPage());
                    this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                };
            }
        }

        private void UserVerificationViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "PopupMessage":
                    this.DisplayAlert("Alert", _userVerificationVM.PopupMessage, "OK");
                    break;
            }
        }
    }
}