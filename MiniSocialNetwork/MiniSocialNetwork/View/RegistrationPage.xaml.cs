﻿using MiniSocialNetwork.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MiniSocialNetwork.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegistrationPage : ContentPage
    {
        private RegistrationViewModel _registrationVM;

        public RegistrationPage()
        {
            InitializeComponent();

            _registrationVM = BindingContext as RegistrationViewModel;
            if (_registrationVM != null)
            {
                _registrationVM.PropertyChanged += RegistrationViewModel_PropertyChanged;
                _registrationVM.NavigateToVerificationPageAction = delegate
                {
                    this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                    App.NavigationPage.PushAsync(new UserVerificationPage());
                    this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                };
            }
        }

        private void RegistrationViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "PopupMessage":
                    this.DisplayAlert("Alert", _registrationVM.PopupMessage, "OK");
                    break;
            }
        }
    }
}