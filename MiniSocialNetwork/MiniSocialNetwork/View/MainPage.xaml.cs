﻿using MiniSocialNetwork.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MiniSocialNetwork.View
{
    public partial class MainPage : ContentPage
    {
        private MainViewModel _mainVM;

        public MainPage()
        {
            InitializeComponent();

            _mainVM = BindingContext as MainViewModel;
            if (_mainVM != null)
            {
                _mainVM.NavigateToLoginPageAction = delegate
                {
                    App.NavigationPage.PushAsync(new LoginPage());
                    this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                };
            }
        }
    }
}
