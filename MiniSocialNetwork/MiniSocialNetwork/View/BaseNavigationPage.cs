﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MiniSocialNetwork.View
{
    public class BaseNavigationPage : NavigationPage
    {
        public BaseNavigationPage()
        {
        }

        public BaseNavigationPage(Page root) 
            : base(root)
        {
        }
    }
}
