﻿using MiniSocialNetwork.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MiniSocialNetwork.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        private LoginViewModel _loginVM;

        public LoginPage()
        {
            InitializeComponent();

            _loginVM = BindingContext as LoginViewModel;
            if (_loginVM != null)
            {
                _loginVM.PropertyChanged += LoginViewModel_PropertyChanged;
                _loginVM.NavigateToMainPageAction = delegate
                {
                    App.NavigationPage.PushAsync(new MainPage());
                    this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
                };
                _loginVM.NavigateToRegisterPageAction = delegate
                {
                    App.NavigationPage.PushAsync(new RegistrationPage());
                };
            }
        }

        private void LoginViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "PopupMessage":
                    this.DisplayAlert("Alert", _loginVM.PopupMessage, "OK");
                    break;
            }
        }
    }
}