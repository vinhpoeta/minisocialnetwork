﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MiniSocialNetwork.Util
{
    public static class FormatUtil
    {
        private const string MAIL_REGEX = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
        private const string PHONE_NUMBER_REGEX = "\\d{4,6}";

        public static bool IsMailAddress(string email)
        {
            return Regex.IsMatch(email, MAIL_REGEX, RegexOptions.IgnoreCase);
        }

        public static bool IsPhoneNumber(string phoneNumber)
        {
            return Regex.IsMatch(phoneNumber, PHONE_NUMBER_REGEX);
        }
    }
}
