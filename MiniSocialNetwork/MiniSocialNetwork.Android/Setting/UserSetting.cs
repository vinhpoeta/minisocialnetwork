﻿using Android.Content;
using Android.Preferences;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static MiniSocialNetwork.Core.Model.Manager.UserManager;

namespace MiniSocialNetwork.Droid.Setting
{
    public class UserSetting : IUserSetting
    {
        private const string user_key_key = "USER_KEY";

        private Context _context;

        public UserSetting(Context context)
        {
            _context = context;
        }

        public string GetUserKey()
        {
            using (var prefs = PreferenceManager.GetDefaultSharedPreferences(_context))
            {
                return prefs.GetString(user_key_key, string.Empty);
            }
        }

        public void SaveUserKey(string key)
        {
            using (var prefs = PreferenceManager.GetDefaultSharedPreferences(_context))
            {
                using (var editor = prefs.Edit())
                {
                    editor.PutString(user_key_key, key);
                    editor.Commit();
                }
            }
        }

        public void DeleteUserKey()
        {
            using (var prefs = PreferenceManager.GetDefaultSharedPreferences(_context))
            {
                using (var editor = prefs.Edit())
                {
                    editor.Remove(user_key_key);
                    editor.Commit();
                }
            }
        }
    }
}
