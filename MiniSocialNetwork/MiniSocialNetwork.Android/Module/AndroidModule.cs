﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Autofac;
using static MiniSocialNetwork.Core.Model.Manager.UserManager;
using MiniSocialNetwork.Droid.Setting;

namespace MiniSocialNetwork.Droid.Module
{
    public class AndroidModule : Autofac.Module
    {
        private Context _context;

        public AndroidModule(Context context)
        {
            _context = context;
        }

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            var userSettings = new UserSetting(_context);
            builder.RegisterInstance<IUserSetting>(userSettings);
        }
    }
}